/*
**--------------------------------------------------------------------------------------
**	File: 					main.js
**	Author: 				Mohau R Quinton Letlala <mrq.letlala@gmail.com>
**	Description:    
**--------------------------------------------------------------------------------------
*/
console.info("Main.js");

// import modules
import "../scss/main.scss";
import './_base/bootstrap.bundle.min';
var jquery = require("jquery");
window.$ = window.jQuery = jquery; // notice the definition of global variables here

// if (/Mobi/.test(navigator.userAgent)) {
//   // mobile!
//   console.log(navigator.userAgent);
// }

$( document ).ready(function() {
  $('#toggle').click(function() {
    $(this).toggleClass('active');
    $('#overlay').toggleClass('open');
    $('body').toggleClass('noscroll');
  });

  // ---------------------------------------------
  // NavBar | Transparency Change onScroll
  // ---------------------------------------------
  var navBar = $('.navbar');
  $(window).on("load scroll resize", checkScroll);

  function checkScroll(){
    var startY = navBar.height() * 2; //The point where the navbar changes in px

    if($(window).scrollTop() > startY){
      navBar.addClass("navbar-scrolled");
    } else {
      navBar.removeClass("navbar-scrolled");
    }
  }
});

export default () => {};