// console.info("Gallery.js");

import main from './main';

// import modules
import 'lightcase';
import '../scss/gallery.scss';

$('a[data-rel^=lightcase]').lightcase({
    transition: 'scrollHorizontal',
    showSequenceInfo: true,
    showTitle: true,
    slideshow: false,
    width: 1000,
    maxWidth: 2000,
    maxHeight: 900,
    useCategories: false
}).sort(function(){
    return Math.random()*10 > 5 ? 1 : -1;
}).each(function(){
    var $t = $(this),
    color = $t.attr("class");
    $t.css({backgroundColor: color}).appendTo( $t.parent() );
});

main();