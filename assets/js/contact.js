/*
**----------------------------------------------------------------------------
**	File: 					contact.js
**	Author: 				Mohau R Quinton Letlala <mrq.letlala@gmail.com>
**	Description:    Custom JavaScript to post contact form to php and accept result
**----------------------------------------------------------------------------
*/
// console.info("Contact.js");

import main from './main';
main();

// import modules
import '../scss/contact.scss';

/**
**---------------------------------------------
** Example starter JavaScript for disabling form submissions if there are invalid fields
**---------------------------------------------
*/
 (function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (!$('.honeypot').val()) {
          // console.log("Honeypot " + $('.honeypot').val());
          // console.log("Human");
        
          if (form.checkValidity() === false) {
            console.log('Check Validity: ' + form.checkValidity());
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
          var data = {};
          data = {
            "email": this.email.value,
            "subject": this.selected.value,
            "name": this.name.value,
            "content": this.content.value
          };

          // console.log(data);
          if (form.checkValidity()) {
            // console.log('Passed');
            event.preventDefault();

            Email.send(data.email, "mrq.letlala@gmail.com", data.subject, data.content, {
              token: "72418fcd-1d68-4414-abe4-32ad8a9373db",
              callback: function done(message) { 
                if (message === "OK") {
                  event.preventDefault();
                  console.info('Successful.');
                  $('#charBreak').hide();
                  $('.alert').show().addClass('alert-success').prepend('<strong class="">Thank you ' + data.name +'!!</strong><br/><br/>Your message has been recieved successfully.');
                  $('.needs-validation').removeClass('was-validated');
                  document.getElementById('contactForm').reset();
                  window.setTimeout(function () {
                    $('.alert').fadeTo(500, 0).slideUp(500, function () {
                      $('.alert').removeClass('alert-success');
                      $('.alert').alert('close');
                    });
                  }, 9000);
                } else {
                  return "Unable to send at the moment, Please again later."
                }
              }
            }); 
          }  
        }
      }, false);
    });
  }, false);


  /*
  **---------------------------------------------
  ** Honeypot
  **---------------------------------------------
  */
  $('.honeypot').hide();

  /*
  **---------------------------------------------
  ** Count Characters in textarea
  **---------------------------------------------
  */
  $.fn.charCount = function ($param) {
    var oldVal = "",
    currentVal = $param.val(),
    max = $param.attr("maxlength"),
    len = currentVal.length,
    char = max - len;

    $('#charNum').show();
    $('#charBreak').show();
    if(currentVal == oldVal) {
      return; //check to prevent multiple simultaneous triggers
    }

    if (len >= max) {
      $('#charNum').text(' you have reached the limit.');
      $('#charNum').addClass('text-danger');
      $('#charNum').removeClass('text-muted');
    } else {
      if (!$param.val) {
        char++;
      }

      $('#charNum').text(' ' + char + ' characters.');
      $('#charNum').removeClass('text-danger');
      $('#charNum').addClass('text-muted');
    }

    return true;
  };

  $('textarea').on('mouseout', function (e) {
    if ($(this).val().length == 0) {
      $('#charNum').hide();
      $('#charBreak').hide();
    }
  }).on('change keyup paste', function (e) { $.fn.charCount($(this)); });
})();


// (function(d, s, id) {
//   var js, fjs = d.getElementsByTagName(s)[0];
//   if (d.getElementById(id)) return;
//   js = d.createElement(s); js.id = id;
//   js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.1';
//   fjs.parentNode.insertBefore(js, fjs);
// }(document, 'script', 'facebook-jssdk'));