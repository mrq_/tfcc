/*
**--------------------------------------------------------------------------------------
**	File: 					main.js
**	Author: 				Mohau R Quinton Letlala <mrq.letlala@gmail.com>
**	Description:    Custom JavaScript to post contact form to php and accept result
**--------------------------------------------------------------------------------------
*/

// console.info("Index.js");
import main from './main';

main();