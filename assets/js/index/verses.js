import { resolve } from "path";
import { rejects } from "assert";

export const verses = {
    getOne: function() {
        return new Promise((resolve, reject) => {
            // fetch('http://quotes.rest/bible/verse.json', {options})
            fetch('https://labs.bible.org/api/?random&type=json', {
                // mode: 'cors',
                // crossDomain: true,
                headers: {
                    "access-control-allow-origin": "*",
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                return resolve(data)
            })
        })
    }
}