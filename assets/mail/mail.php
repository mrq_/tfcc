<?php
/*
**------------------------------------------------------------------------------------------
**  File:           server/mail.php
**  Author:         Mohau R Quinton Letlala <mrq.letlala@gmail.com>
**  Description:    Custom php to e-mail message from contact form to site admin
**------------------------------------------------------------------------------------------
*/

    header("Content-Type: application/json");
  // header('Access-Control-Allow-Origin: http://www.example.com/');
  header('Access-Control-Max-Age: 3628800');
  header('Access-Control-Allow-Methods: POST');
?>

<?php
	$email = $_POST["email"];
	$subject = $_POST["subject"];
	$name = $_POST["name"];
	$message = $_POST["content"];

	$data = new \stdClass();

	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion();
	$headers .= "X-Priority: 1" . "\r\n"; 

	// More headers
	$headers .= "From: ". $email . "\r\n";
	// $headers .= "Bcc: myboss@example.com" . "\r\n";


	$to = "mrq.letlala@gmail.com";
	$headers = "From: " . $email;
	$subject = "TFCC Web Page: " . $subject;
	$body = "Name: \t" . $name . "\n";
	$body .= "E-mail address: \t " . $email . "\n\n";
	$body .= "Message: \n\n";
	$body .= $message;

	if (mail($to, $subject, $body, $headers)) {
		$data->status = true;
    // $data['posted'] = 'Data Was Posted Successfully';
	} else {
		$data->status = false;
	}

	echo json_encode($data);
?>